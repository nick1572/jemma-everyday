<?php

 /* 
  Template Name: Blog Template
*/

 get_header(); 

global $post;
//$category_id = get_the_category($post->ID)[0]->term_id;
 ?>

 <div class="primary" style="margin-bottom:2rem;">
    <?php $query = new WP_Query( array( 'category_name' => 'news-articles' ) ); ?>
  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

  <div class="news-article">
            <div class="post">
                <div class="post-content">
                    <h2 class="widget-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </div>

<?php endwhile; ?>

  <!--<div class="navigation">
    <div class="next-posts"><?php //next_posts_link(); ?></div>
    <div class="prev-posts"><?php //previous_posts_link(); ?></div>
  </div>-->

<?php else : ?>

  <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
    <h1>Posts Not Found</h1>
  </div>

<?php endif; ?>
<?php wp_reset_postdata(); ?>
</div>
<?php
get_sidebar();
get_footer();