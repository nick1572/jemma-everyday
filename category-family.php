<?php


get_header(); 

global $post;
$category_id = get_the_category($post->ID)[0]->term_id;
/*echo '<pre>';
var_dump($category_id);
echo '</pre>';
*/

?> 

<div class="primary-full">


<?php query_posts(array('cat' => '12', "posts_per_page" => '15')); ?>
	<?php $count = 1;?>
	<?php if (have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
      
	   
        <div class="post-container">
            <div class="post">
               <?php if ( has_post_thumbnail() ) : ?>
                <a href="<?php the_permalink(); ?>">
                <div class="post-thumb <?php if($category_id) { echo 'purple-corner';} ?>">
                   <div class="post-corner"></div>
                   
                   <div class="post-cover">
                   
                    <?php if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
                       <p><?php echo $caption; ?></p>

                    <?php endif ; ?>               
    
                    </div>
                
                     <?php the_post_thumbnail();?>
                </div>
                </a>
                <?php endif ; ?>
                <div class="post-content">
                    <h2 class="widget-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </div>
        
        <?php if( 2 === $count): ?>
        
            <div class="partners">
            <div class="slider-row">
                <?php dynamic_sidebar('partners') ?>   
            </div>
        </div>


       
        
        
        
        <?php endif; ?>
        
      
        <?php $count++; ?>
        
	
	<?php endwhile; wp_reset_postdata(); else : ?>
	<?php endif; ?>
	

	
	
	
	

</div>




<?php get_footer(); ?>