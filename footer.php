<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jemma_Everyday
 */

?>

			</div><!-- #content -->
		</main><!--Main-->
	</div><!-- #page -->

	<?php get_template_part( 'template-parts/explore-more-items', '');?>
	<?php get_template_part( 'template-parts/signup', '');?>

	<footer id="colophon" class="footer" role="contentinfo">
	<div class="site-footer">

		<?php dynamic_sidebar('footer-widget-area'); ?>
	



		

		</div>
		<div class="site-info">
			Jemma Everyday 
			<span class="sep"> | </span>	
			Copyright 2016 | <a href="<?php echo get_the_permalink(2016); ?>">Privacy Policy</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->


<?php wp_footer(); ?>

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(101010592); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/101010592ns.gif" /></p></noscript>
</body>
</html>
