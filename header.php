<?php
/**
* The header for our theme.
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
		*
		* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
		*
		* @package Jemma_Everyday
		*/
	?><!DOCTYPE html>
	<html <?php language_attributes(); ?>>
		<head>
			<meta charset="<?php bloginfo( 'charset' ); ?>">
			<title><?php wp_title(''); ?></title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="profile" href="http://gmpg.org/xfn/11">
			<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
			<link rel="stylesheet" media="print" href="<?php bloginfo('stylesheet_directory');?>/assets/css/print.css">
			<link href="<?php bloginfo('stylesheet_directory');?>/assets/css/wp-styles.css" rel="stylesheet">
			<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/assets/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/assets/css/jquery-ui.min.css">
			<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/assets/css/magnific-popup.css">

			
			<!-- Facebook Pixel Code -->
			<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window,document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1602782533364524');
			fbq('track', 'PageView');
			</script>
			<noscript>
			<img height="1" width="1"
			src="https://www.facebook.com/tr?id=1602782533364524&ev=PageView
			&noscript=1"/>
			</noscript>
			<!-- End Facebook Pixel Code -->
			
			<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-85364570-1', 'auto');
			ga('send', 'pageview');
			</script>
			
			<?php wp_head(); ?>
		</head>
		<body <?php body_class(); ?>>
			<div id="page" class="site">
				<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'jemma_ev' ); ?></a>
				<header id="masthead" class="site-header" role="banner">
					<div class="site-branding">
						<div class="logo">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<img src="<?php echo get_theme_mod('jemma_logo'); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
							</a>
						</div>
						<div class="header-actions">
							<div class="jemma-everyday-social-links">
								<?php dynamic_sidebar('header-social-links'); ?>
							</div>
							<div class="jemma-financial-link">
								<?php dynamic_sidebar('header-inner-links'); ?>
							</div>
						</div>
						<div class="jemma-band">
							<?php dynamic_sidebar('header-band-call'); ?>
							
						</div>
						</div><!-- .site-branding -->
						</header><!-- #masthead -->
						<!--<div><strong>Current template:</strong> <?php //get_current_template( true ); ?></div>-->
						<?php  get_template_part('template-parts/nav', '');
						if(is_front_page()) {
							get_template_part('template-parts/subheaders/subheader', 'front-new');
						}
						elseif(is_page('meet-us')) {
							get_template_part('template-parts/subheaders/subheader', 'meetus');
						}
						elseif(is_home('news') || in_category($category = 'news-articles')) {
							get_template_part('template-parts/subheaders/subheader', 'news');
						}
						elseif(is_page('our-partners')) {
							get_template_part('template-parts/subheaders/subheader', 'our-partners');
						}
						elseif(is_page('videos')) {
							get_template_part('template-parts/subheaders/subheader', 'video-archives');
						}
						elseif(is_page('contact-us')) {
							get_template_part('template-parts/subheaders/subheader', 'contact-us');
						}
						elseif(is_page('brave-and-beyond')){
							get_template_part('template-parts/subheaders/subheader', 'bb');
						}
						elseif(is_category($category = 'money') || in_category('money')){
							get_template_part('template-parts/subheaders/subheader', 'money');
						}
						elseif(is_category($category = 'family') || in_category('family')){
							get_template_part('template-parts/subheaders/subheader', 'family');
						}
						elseif(is_category($category = 'work') || in_category('work')){
							get_template_part('template-parts/subheaders/subheader', 'work');
						}
						elseif(is_category($category = 'life') || in_category('life')){
							get_template_part('template-parts/subheaders/subheader', 'life');
						}
						elseif(is_author()){
							get_template_part('template-parts/subheaders/subheader', 'life');
						}
						
						elseif(is_single('financial-birthday-checklist')){
							get_template_part('template-parts/subheaders/subheader', 'birthday');
						}
						?>
						<main class="main" role="main">
						<div id="content" class="site-content">