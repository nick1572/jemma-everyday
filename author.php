<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package test
 */

get_header(); ?>


<div class="primary" style="min-height: 600px;">

<!-- This sets the $curauth variable -->

    <?php
    	global $coauthors_plus;
			if (isset($coauthors_plus)) {
				$curauth = $coauthors_plus->get_coauthor_by( 'user_nicename', $author_name );
			} else {
   			if(isset($_GET['author_name'])) :
      			$curauth = get_userdatabylogin($author_name);
      		else :
      			$curauth = get_userdata(intval($author));
  			endif;
			}

    ?>

    <article class="author-content">
        <div class="author-entry-intro">
        <?php if (!coauthors_get_avatar( $curauth->ID ) ) {
                  echo '<h2>'.$curauth->display_name. '</h2>';

              } else {
                 echo coauthors_get_avatar( $curauth, 170 );
              }

              ?>
          
                
  

    
           

            <?php if($curauth->description): ?> 
      
            <div class="author-details">
                

                <p><?php echo $curauth->description;?></p>
            </div>
        <?php endif;?>
        </div>

   <div class="author-entries">
   <p class="articles-by">Articles By: <?php echo $curauth->display_name;?></p>
    <ul>
<!-- The Loop -->

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <li>
            <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
            <?php the_title(); ?></a>
            <br><span class="author-entry-meta">Posted on
            <?php the_time('d M Y'); ?> in, <?php the_category('&');?></span>
        </li>

    <?php endwhile; else: ?>
        <p><?php _e('No posts by this author.'); ?></p>

    <?php endif; ?>

<!-- End Loop -->

    </ul>
    </div>
    </article>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

