<div id="accordion">
<div class="accordionheader">
<h2>Want To Get In Touch? <i class="fa fa-angle-right"></i></h2>
</div>
<div>[formidable id=8]</div>
<div class="accordionheader">
<h2>Want To Be a Corporate Partner? <i class="fa fa-angle-right"></i></h2>
</div>
<div>

Looking to become a Jemma corporate partner and have your company or organization showcased to the national women’s market?  <strong>Please contact Bonnie Stein at 443.652.4201.</strong>

</div>
<div class="accordionheader">
<h2 class="last-header">Submit Nominee to <img class="alignnone  wp-image-56 inline-header-image" src="http://localhost/jemmaeveryday/wp-content/uploads/2016/09/brave-and-beyond-4c-web-e1473705196813.png" alt="brave-and-beyond-4c-web" width="136" height="38" /><i class="fa fa-angle-right"></i></h2>
</div>
<div><strong>Simply follow the steps below:</strong>
<ul class="fa-ul">
 	<li><i class="fa fa-li fa-angle-double-right"></i> Use your phone to record a 30-second video telling us why you would like to nominate your Brave &amp; Beyond woman.</li>
 	<li><i class="fa fa-li fa-angle-double-right"></i> Please include your name, email address and phone number in the video.</li>
 	<li> <i class="fa fa-li fa-angle-double-right"></i> Submit your video by clicking the link below and following the instructions provided.</li>
</ul>
<a href="https://www.dropbox.com/request/1OmDgrU0J24dP5PgrAPY?oref=e" class="contact-nominee-submit" target="_blank">Submit</a>
</div>
</div>