jQuery.noConflict();
(function($) {


    $('.entry-content ul').addClass("fa-ul");
    $('.entry-content ul li').prepend('<i class="fa fa-li fa-angle-double-right"></i>');
    $("#field_9psly, #field_5hgn6").prepend("<option value='' selected='selected'>Select Your State</option>");



    $('.menu-toggle').on('click', function() {
        $('.menu').toggleClass('expanded');

    });

    /*-----JQuery Accordion-----*/

    $(function() {
        $(".accordion").show();
        var $accordions = $(".accordion").accordion({
            collapsible: true,
            active: false,
            icons: false
        }).on('click', function() {
            $accordions.not(this).accordion("option", "active", 1);
        });
    });


    /*-----Print This-----*/




    /*-----Simple Modal Popup-----*/

    $('.popup-link').magnificPopup({
        type: 'inline'
    });

    $('.signup-popup-link').magnificPopup({
        type: 'inline'
    });


    /*$('.popup-vimeo').magnificPopup({
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler video-resize">' +
                '<div class="mfp-close"></div>' +
                '<iframe class="mfp-iframe" width="640" height="360" frameborder="0" allowfullscreen> </iframe>' +
                '</div>'
        }
    });*/

    $('.popup-youtube, .popup-vimeo').magnificPopup({
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">' +
                '<div class="mfp-close"></div>' +
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button

            patterns: {
                youtube: {
                    index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

                    id: 'v=', // String that splits URL in a two parts, second part should be %id%
                    // Or null - full URL will be returned
                    // Or a function that should return %id%, for example:
                    // id: function(url) { return 'parsed id'; }

                    src: 'https://www.youtube.com/embed/%id%?autoplay=1&;rel=0' // URL that will be set as a source for iframe.
                },
                vimeo: {
                    index: 'vimeo.com/',
                    id: '/',
                    src: '//player.vimeo.com/video/%id%?autoplay=1'
                },
                gmaps: {
                    index: '//maps.google.',
                    src: '%id%&output=embed'
                }

                // you may add here more sources

            },

            srcAction: 'iframe_src', // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
        }
    });



    $(document).on('click', '.popup-modal-dismiss, .popup-modal-dismiss-button', function(e) {
        var magnificPopup = $.magnificPopup.instance;
        e.preventDefault();
        magnificPopup.close();
    });


    $(document).on('click', '.closePopup', function(e) {
        e.preventDefault();
        $.magnificPopup.close();
    });



    var slides = document.getElementById("thumbnail-slider").getElementsByTagName("li");
    for (var i = 0; i < slides.length; i++) {
        slides[i].onmouseover = function(e) {
            var li = this;
            if (li.thumb) {
                var content = "<div class='tip-wrap'><div class='tip-text'>" + li.thumb.innerHTML + "</div></div>";
                tooltip.pop(li, content);
            }
        };
    }

    /*
      These are the link replacements for the slider
      The current function of the slider is using an 'a' tag so we 
      need to replace a span here to link somewhere else
    */

    $('.addlink').replaceWith(function() {
        var url = 'http://www.hopkinsmedicine.org/patient_care/travel-for-care';
        return '<a style="color:white;text-decoration:underline;" href="' + url + '" target="_blank">Travel For Care website</a>.';
    });

    $('.addrakkoonlink').replaceWith(function() {
        var url = 'https://www.rakkoon.com/';
        return '<a style="color:white;text-decoration:underline;" href="' + url + '" target="_blank">RAKKOON.com</a>.';
    });


    /* US Lacrosse*/

    $('.add-us-archive-link').replaceWith(function() {
        var url = 'https://jemmaeveryday.com/author/us-lacrosse/';
        return '<a style="color:white;text-decoration:underline;display:block;font-weight:bold;" href="' + url + '">View Partner Content.</a>';
    });

    /* McCormick*/

    $('.add-mc-archive-link').replaceWith(function() {
        var url = 'https://jemmaeveryday.com/author/mccormick/';
        return '<a style="color:white;text-decoration:underline;display:block;font-weight:bold;" href="' + url + '">View Partner Content.</a>';
    });

    /* RAKKOON*/

    $('.add-rak-archive-link').replaceWith(function() {
        var url = 'https://jemmaeveryday.com/author/rakkoon/';
        return '<a style="color:white;text-decoration:underline;display:block;font-weight:bold;" href="' + url + '">View Partner Content.</a>';
    });

    /* GBMC*/

    $('.add-gbmc-archive-link').replaceWith(function() {
        var url = 'https://jemmaeveryday.com/author/gbmc/';
        return '<a style="color:white;text-decoration:underline;display:block;font-weight:bold;" href="' + url + '">View Partner Content.</a>';
    });

    /* EIF */

    $('.add-eif-archive-link').replaceWith(function() {
        var url = 'https://jemmaeveryday.com/author/excellence-in-fitness/';
        return '<a style="color:white;text-decoration:underline;display:block;font-weight:bold;" href="' + url + '">View Partner Content.</a>';
    });

    /*JHM */

    $('.add-jhm-archive-link').replaceWith(function() {
        var url = 'https://jemmaeveryday.com/author/hopkins-medicine/';
        return '<a style="color:white;text-decoration:underline;display:block;font-weight:bold;" href="' + url + '">View Partner Content.</a>';
    });

   



})(jQuery);
