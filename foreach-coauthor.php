<?php $coauthors = get_coauthors();
            foreach($coauthors as $coauthor):
                $archive_link = get_author_posts_url( $coauthor->ID, $coauthor->user_nicename );
                $link_title   = $coauthor->display_name;
                $description  = $coauthor->description;

            ?>
                <div class="">
                <?php echo coauthors_get_avatar( $coauthor, 65 ); ?>
                <a href="<?php echo $archive_link;?>" "email me"><?php echo $link_title ?></a>
                <p><i><?php echo  $description;?></i></p>
                </div>
            <?php endforeach;
          ?>



<div class="author">
    <?php 
        $coauthors = get_coauthors();
        foreach ($coauthors as $coauthor): ?>
    <?php 
        $archive_link = get_author_posts_url($coauthor->ID, $coauthor->user_nicename);
        $link_title   = $coauthor->display_name;
        $description  = $coauthor->description;

    ?>
    <?php if ( has_post_thumbnail($coauthor->ID)): ?>
       <div class="author-thumb">
            <?php echo coauthors_get_avatar( $coauthor, 120 ); ?>
        </div>
    <?php else: echo '';
        endif; 
    ?>
        <div class="author-info">
            <a href="<?php echo $archive_link; ?>"><?php echo $link_title; ?></a>
            <p class="sub-title"><i><?php echo  $description;?></i></p>
        </div>
    <?php endforeach; ?>
</div>




<?php $authorImage = get_field('author_image'); ?>
   <?php $authorDetails = get_field('author_details'); ?>  


    <?php if(!empty($authorImage) || !empty($authorDetails)): ?>

    <div class="author">
        <?php if(!empty($authorImage)): ?>
        <div class="author-thumb"><img src=""/></div>
        <?php endif ;?>
        <?php if(!empty($authorDetails)) :?>
        <div class="author-info">
        </div>
        <?php endif ;?>
    </div>
    <?php else: echo '';?>
    <?php endif; ?>










if there is a coauthor then show the image, link and description

if there is no image then show link and description
