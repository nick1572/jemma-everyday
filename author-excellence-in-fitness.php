<?php
/**
 * The template for displaying Main Author Specific archive pages.
 *  Co Author templates use the general WP author template...
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package test
 */

get_header(); ?>





<div class="primary" style="min-height: 600px;">
    <article class="author-content">
        <div class="author-entry-intro">
          <?php
            $user = get_user_by('slug', $author_name);
            echo get_wp_user_avatar($user->ID, 140);
          ?>
        
            <p><?php echo $user->description;?></p>
        
        </div>
   <div class="author-entries">
     <p class="articles-by">Articles By: Excellence In Fitness</p>
    <ul>
<!-- The Loop -->

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <li>
            <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
            <?php the_title(); ?></a>
            <br><span class="author-entry-meta">Posted on
            <?php the_time('d M Y'); ?> in, <?php the_category('&');?></span>
        </li>

    <?php endwhile; else: ?>
        <p><?php _e('No posts by this author.'); ?></p>

    <?php endif; ?>

<!-- End Loop -->

    </ul>
    </div>
    </article>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

