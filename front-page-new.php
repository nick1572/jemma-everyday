<?php
/*
Template Name: Front-Page Test
*/
get_header('new');
global $post;
?>
<?php
$image_path = wp_upload_dir();
?>
<div class="primary-full">
<div class="post-container">
    <?php
        while ( have_posts() ) : the_post();
            get_template_part( 'template-parts/content', 'page' );
        endwhile; // End of the loop.
    ?>
    </div>
</div><!--//End Primary Full-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>