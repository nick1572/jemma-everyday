<?php
/**
 * Jemma Everyday Theme Customizer.
 *
 * @package Jemma_Everyday
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function jemma_ev_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

}
add_action( 'customize_register', 'jemma_ev_customize_register' );


// Add Logo Option to the Customizer

function add_jemma_logo($wp_customize) {
	$wp_customize->add_setting('jemma_logo');
	$wp_customize->add_control(new WP_Customize_Image_Control ($wp_customize, 'jemma_logo', 
	array(
		'label'    => 'Upload Logo',
		'section'  => 'title_tagline',
		'settings' => 'jemma_logo'

		)
	));
}

add_action('customize_register', 'add_jemma_logo');

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function jemma_ev_customize_preview_js() {
	wp_enqueue_script( 'jemma_ev_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'jemma_ev_customize_preview_js' );

