<?php


get_header(); 
global $post;

?>

<?php
    $image_path = wp_upload_dir();
?>


    <div class="primary-full">


        <div class="post-container">
            <div class="post">
                <div class="post-thumb">
                    <div class="post-corner"></div>

                    <div class="post-cover">
                        <?php if ( $caption = get_post( get_post_thumbnail_id(281) )->post_excerpt ) : ?>
                            <p>
                                <a href="<?php the_permalink(281); ?>">
                                    <?php echo $caption; ?>
                                </a>
                            </p>
                            <?php endif ; ?>
                    </div>
                    <?php echo get_the_post_thumbnail(281);?>
                </div>

                <div class="post-content">
                    <h2 class="widget-title"><a href="<?php the_permalink(281); ?>"><?php echo get_the_title(281 );?></a></h2>
                    <p>
                        “Gender does not define you as an athlete: your grit, determination, hard work, and passion for the game do.” This great quote from a collegiate lacrosse player, Annie Spewak, in a recent essay on perceptions...
                        <a href="<?php the_permalink(281); ?>">Read more &raquo;</a>
                    </p>
                </div>
            </div>
        </div>


        <div class="post-container">
            <div class="post">
                <div class="post-thumb">
                    <div class="post-corner"></div>

                    <div class="post-cover">
                        <?php if ( $caption = get_post( get_post_thumbnail_id(582) )->post_excerpt ) : ?>
                            <p>
                                <a href="<?php the_permalink(582); ?>">
                                    <?php echo $caption; ?>
                                </a>
                            </p>
                            <?php endif ; ?>
                    </div>
                    <?php echo get_the_post_thumbnail(582);?>
                </div>

                <div class="post-content">
                    <h2 class="widget-title"><a href="<?php the_permalink(582); ?>"><?php echo get_the_title(582 );?></a></h2>
                    <p>
                        Although October is breast cancer awareness month, any time is a good time to speak with a physician about breast cancer. According to the American Cancer Society (ACS), about one in eight women in the United States will...
                        <a href="<?php the_permalink(582); ?>">Read more &raquo;</a>
                    </p>
                </div>
            </div>
        </div>



        <div class="partners">
            <div class="slider-row">
                <?php dynamic_sidebar('partners') ?>
            </div>
        </div>


        <div class="post-container">

            <div class="featured-post-third">
                <div class="featured-thumb-third dk-purple-corner">
                    <div class="post-corner"></div>
                    
                                   <div class="post-cover">
                   
                   <?php if ( $caption = get_post( get_post_thumbnail_id(576) )->post_excerpt ) : ?>
                            <p>
                                <a href="<?php the_permalink(576); ?>">
                                    <?php echo $caption; ?>
                                </a>
                            </p>
                            <?php endif ; ?>
                    </div>      

                    
                    

                    <a href="<?php the_permalink(576); ?>"> <?php echo get_the_post_thumbnail(576);?></a>

                </div>
                <div class="featured-title">
                    <h2 class="widget-title"><a href="<?php the_permalink(576); ?>"><?php echo get_the_title(576 );?></a></h2>
                </div>
            </div>
            
             <div class="featured-post-third">
                <div class="featured-thumb-third blue-corner">
                    <div class="post-corner"></div>
                    
                    
                     <div class="post-corner"></div>
                    
                                   <div class="post-cover">
                   
                   <?php if ( $caption = get_post( get_post_thumbnail_id(378) )->post_excerpt ) : ?>
                            <p>
                                <a href="<?php the_permalink(378); ?>">
                                    <?php echo $caption; ?>
                                </a>
                            </p>
                            <?php endif ; ?>
                    </div>      

                    

                    <a href="<?php the_permalink(378); ?>"> <?php echo get_the_post_thumbnail(378);?></a>

                </div>
                <div class="featured-title">
                    <h2 class="widget-title"><a href="<?php the_permalink(378); ?>"><?php echo get_the_title(378);?></a></h2>
                </div>
            </div>
            
             <div class="featured-post-third">
                <div class="featured-thumb-third purple-corner">
                    <div class="post-corner"></div>
                    
                    
                     <div class="post-corner"></div>
                    
                                   <div class="post-cover">
                   
                   <?php if ( $caption = get_post( get_post_thumbnail_id(521) )->post_excerpt ) : ?>
                            <p>
                                <a href="<?php the_permalink(521); ?>">
                                    <?php echo $caption; ?>
                                </a>
                            </p>
                            <?php endif ; ?>
                    </div>      


                    <a href="<?php the_permalink(521); ?>"> <?php echo get_the_post_thumbnail(521);?></a>

                </div>
                <div class="featured-title">
                    <h2 class="widget-title"><a href="<?php the_permalink(521); ?>"><?php echo get_the_title(521 );?></a></h2>
                </div>
            </div>






        </div>
        
        
        
        
        <div class="post-container">

            <div class="featured-post-half">
                <div class="featured-thumb-third blue-corner">
                    <div class="post-corner"></div>
                    
                                   <div class="post-cover">
                   
                   <?php if ( $caption = get_post( get_post_thumbnail_id(483) )->post_excerpt ) : ?>
                            <p>
                                <a href="<?php the_permalink(483); ?>">
                                    <?php echo $caption; ?>
                                </a>
                            </p>
                            <?php endif ; ?>
                    </div>      

                    
                    

                    <a href="<?php the_permalink(483); ?>"><img src="<?php echo $image_path['baseurl']; ?>/featured-images/header-main-480x270.jpg"></a>

                </div>
                <div class="featured-title">
                    <h2 class="widget-title"><a href="<?php the_permalink(483); ?>"><?php echo get_the_title(483 );?></a></h2>
                </div>
            </div>
            
             <div class="featured-post-half">
                <div class="featured-thumb-third dk-purple-corner">
                    <div class="post-corner"></div>
                    
                    
                     <div class="post-corner"></div>
                    
                                   <div class="post-cover">
                   
                   <?php if ( $caption = get_post( get_post_thumbnail_id(371) )->post_excerpt ) : ?>
                            <p>
                                <a href="<?php the_permalink(371); ?>">
                                    <?php echo $caption; ?>
                                </a>
                            </p>
                            <?php endif ; ?>
                    </div>      

                    

                    <a href="<?php the_permalink(371); ?>"><img src="<?php echo $image_path['baseurl']; ?>/featured-images/header-main-480x270-2.jpg"></a>

                </div>
                <div class="featured-title">
                    <h2 class="widget-title"><a href="<?php the_permalink(371); ?>"><?php echo get_the_title(371);?></a></h2>
                </div>
            </div>
            
          






        </div>










    </div>



    <?php get_sidebar(); ?>
        <?php get_footer(); ?>
