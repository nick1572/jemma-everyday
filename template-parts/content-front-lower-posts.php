<?php
global $post;
$category_id = get_the_category($post->ID)[0]->term_id;
//var_dump($category_id);
?>

<div class="post">
    
    <a href="<?php the_permalink(); ?>">
    <div class="post-thumb <?php if($category_id == 10) { echo 'blue-corner';} elseif($category_id == 1) { echo 'gray-corner';} elseif ($category_id == 12) { echo 'purple-corner';} elseif ($category_id == 11 /*dark purple for life? should be 18*/) { echo 'dk-purple-corner';} else { echo '';} ?>">
        <div class="post-corner"></div>
        <div class="post-cover">
            <?php if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
            <p><?php echo $caption; ?></p>
            <?php endif ; ?> 
        </div>
        <?php if(has_post_thumbnail()) { echo get_the_post_thumbnail();} ?>
    </div>
    </a>

    <div class="post-content">
        <h2 class="widget-title"><a href="<?php the_permalink(); ?>"><?php echo get_the_title();?></a></h2>
        <p><?php the_excerpt();?>
        </p>
    </div>
</div>