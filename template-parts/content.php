<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jemma_Everyday
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	
    <?php $image = get_field('sponsor_image');?>
    <?php $text  = get_field('sponsor_text');?>
    <?php $url   = get_field('sponsor_url'); ?>
    
    <?php
     /*
        Check if either of the image or text is not empty
     */
    ?>
    <?php if(!empty($image) || !empty($text)): ?>
       <div class="sponsor" style="margin-bottom:1.5rem;">
            <div class="left-sponsor-logo">
            <?php if(!empty($url)): ?>
                <a href="<?php echo $url; ?>" target="_blank">
            <?php endif; ?>
                    <img src="<?php echo $image['url'] ?>"></a>
            </div>
            <div class="right-sponsor-content"><p><?php echo $text;?></p></div>
        </div>
    <?php endif; ?>


    <?php
        if ( is_single() ) :
        the_title( '<h1 class="entry-title" style="margin-bottom:0.5rem !important;">', '</h1>' );
        else :
        the_title( '<h2 class="entry-title" ><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        endif;
       
    ?>
       <?php jemma_ev_posted_on(); ?> 
   
   
   <?php $authorImage = get_field('author_image'); ?>
   <?php $authorDetails = get_field('author_details'); ?>  


    <?php if(!empty($authorImage) || !empty($authorDetails)): ?>

    <div class="article-author">
        <?php if(!empty($authorImage)): ?>
        <div class="author-thumb"><img src="<?php echo $authorImage; ?>"/></div>
        <?php endif ;?>
        <?php if(!empty($authorDetails)) :?>
        <div class="author-info"><?php echo $authorDetails; ?></div>
        <?php endif ;?>
    </div>
    <?php else: echo '';?>
    <?php endif; ?>

    




	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'jemma_ev' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jemma_ev' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->




	<footer class="entry-footer">
		<?php //jemma_ev_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
