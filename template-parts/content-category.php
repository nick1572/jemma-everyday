<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jemma_Everyday
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<div class="entry-content">
	
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php //jemma_ev_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->