<nav id="site-navigation" role="navigation">
		<div class="menu-toggle"><button><i class="fa fa-bars" aria-hidden="true"></i> Menu</button></div></button>
		<div class="primary-navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'walker' => new Column_Walker ) ); ?>
			</div>
</nav><!-- #site-navigation -->
