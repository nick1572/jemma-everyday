<?php

/**
 * Template part for displaying page content in videos.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jemma_Everyday
 */

global $post;
//$category_id = get_the_category($post->ID)[0]->term_id;
 ?>
 <div class="primary-full" style="margin-bottom:2rem;">
    <?php $query = new WP_Query( array( 'tag' => 'video' ) ); ?>
  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

  <div class="video-archive-container">
            <div class="post">
               <?php if ( has_post_thumbnail() ) : ?>
               <div class="post-thumb">
                   <div class="post-corner"></div>
                   
                  <div class="post-cover">
                   
                    <?php if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
                       <p><a href=""><?php echo $caption; ?></a></p>

                     <?php else: ?>
                          <p class="thumb-generic-linker"><a href=""><i class="fa fa-link" aria-hidden="true"></i></a></p>
                    <?php endif ; ?>               
    
                    </div>
                
                     <?php the_post_thumbnail();?>
                </div>
                <?php endif ; ?>
                <div class="post-content">
                    <h2 class="widget-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    
                </div>
            </div>
        </div>

<?php endwhile; ?>

  <!--<div class="navigation">
    <div class="next-posts"><?php //next_posts_link(); ?></div>
    <div class="prev-posts"><?php //previous_posts_link(); ?></div>
  </div>-->

<?php else : ?>

  <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
    <h1>Posts Not Found</h1>
  </div>

<?php endif; ?>
<?php wp_reset_postdata(); ?>
</div>
