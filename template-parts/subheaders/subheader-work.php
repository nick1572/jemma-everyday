<?php 
 $image_path = wp_upload_dir();
if(is_category('work')): ?>
<div class="subheader">
    <div class="hero large-gray-corner">
        <div class="post-corner"></div>
        <div class="hero-left-interior" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/work-2016-bg.jpg)">
            <div class="inner-hero-content">
                <h1>
                <?php if (is_single()) {
                echo '';
                } else {
                single_cat_title();
                }
                ?>
                
                </h1>
            </div>
        </div>
        <div class="hero-right">
            <?php if ( is_active_sidebar( 'jemma-module' ) ) : ?>
            <?php dynamic_sidebar('jemma-module'); ?>
            <?php endif; ?>
        </div>
    </div>
</div>


<?php elseif(is_single('what-to-do-when-your-company-is-acquired')):?>

<div class="subheader">
    <div class="hero large-gray-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/work-headers/W-0009.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif(is_single('five-salary-negotiation-tips-women')):?>

<div class="subheader">
    <div class="hero large-gray-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/work-headers/W-0008.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('how-to-alleviate-stress-when-work-is-keeping-you-up-night')):?>

<div class="subheader">
    <div class="hero large-gray-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/work-headers/W-0007.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('getting-your-foot-in-the-door-tips-for-networking')):?>

<div class="subheader">
    <div class="hero large-gray-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/work-headers/W-0004.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif(is_single('easy-steps-to-improve-your-resume-and-cover-letter')):?>

<div class="subheader">
    <div class="hero large-gray-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/work-headers/W-0001.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php // Checking if author page, then show another subheader. WP keeps pulling the wrong file :P
    elseif(is_author()): 
    get_template_part('template-parts/subheaders/subheader', 'author');
?>


<?php else: ?>
    
<div class="subheader">
    <div class="hero large-gray-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/header-work-full.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php endif; ?>
