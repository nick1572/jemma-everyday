<?php 
 $image_path = wp_upload_dir();
if(is_category('money')): ?>
<div class="subheader">
    <div class="hero large-blue-corner">
        <div class="post-corner"></div>
        <div class="hero-left-interior" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-2016-bg.jpg)">
            <div class="inner-hero-content">
                <h1>
                <?php if (is_single()) {
                echo '';
                } else {
                single_cat_title();
                }
                ?>
                
                </h1>
            </div>
        </div>
        <div class="hero-right">
            <?php if ( is_active_sidebar( 'jemma-module' ) ) : ?>
            <?php dynamic_sidebar('jemma-module'); ?>
            <?php endif; ?>
        </div>
    </div>
</div>


<?php elseif(is_single('holiday-budget-busters-you-might-not-consider')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/dec2016/M-0042-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif(is_single('pay-off-debt-or-invest')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/2016/12/M-0004-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif(is_single('psychology-behind-saving-for-retirement')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/dec2016/M-0031-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif(is_single('grandparents-be-careful-with-529-plans')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/dec2016/M-0010-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif(is_single('quick-tips-student-loans')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/dec2016/M-0019-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif(is_single('small-money-saving-habits')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/nov2016/M-0006-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('leasing-a-car')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/nov2016/M-0012-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('raising-financially-savvy-kids')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/M-0030-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif(is_single('the-power-of-saving-for-retirement-in-your-early-20s')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/M-0018-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('words-to-live-by-start-saving-now')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/M-0001-Comp.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('easing-the-pain-of-student-debt')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/M-0025-Comp.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('why-your-401k-matters-more-than-you-think')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/M-0021-Comp.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('emergency-funds-just-how-much-is-enough')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/M-0026-Comp.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('three-little-numbers-that-can-change-your-life')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/M-0024-Comp.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('kiss-sallie-mae-good-bye-five-years-or-less')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/M-0013-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif(is_single('beware-of-vanishing-money')): ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/money-headers/M-0022-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php // Checking if author page, then show another subheader. WP keeps pulling the wrong file :P
    elseif(is_author()): 
    get_template_part('template-parts/subheaders/subheader', 'author');
?>

<?php else: ?>

<div class="subheader">
    <div class="hero large-blue-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/header-money-full.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php endif; ?>

