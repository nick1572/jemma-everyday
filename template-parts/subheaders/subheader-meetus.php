<?php
    $image_path = wp_upload_dir();
?>
<div class="subheader">
    <div class="hero">
        <div class="hero-left-interior" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/meetus-2016-bg.jpg)">
            <div class="inner-hero-content">
            <?php 
                if(is_page('meet-us')) {
                    echo '<h1>'. get_the_title();'</h1>';
                } elseif(is_page('contact-us')) {
                     echo '<h1>Contact Us</h1>';
                } elseif(is_category('work')) {
                     echo '<h1>Work</h1>';
                } elseif(is_category('life')) {
                     echo '<h1>Life</h1>';
                }
            
            ?>
            </div>
        </div>
        <div class="hero-right">
            <?php if ( is_active_sidebar( 'jemma-module' ) ) : ?>
               
                    <?php dynamic_sidebar('jemma-module'); ?>
               

            <?php endif; ?>
        </div>
    </div>
</div>

