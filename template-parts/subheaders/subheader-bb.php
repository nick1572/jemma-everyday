<?php
    $image_path = wp_upload_dir();
?>
<div class="subheader">
    <div class="hero" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/bb-new-sub1.png)">
        <div class="hero-left-interior">
        <div class="inner-hero-content">
            <h1>Brave &amp; Beyond</h1>
        </div>
        </div>
    </div>
</div>
