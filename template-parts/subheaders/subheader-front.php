<div class="subheader">
    <div class="hero">
        <div class="hero-left">
        	<iframe style="" src="https://player.vimeo.com/video/183540702" width="672" height="382" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <div class="hero-right">
            <?php if ( is_active_sidebar( 'jemma-module' ) ) : ?>
                
                    <?php dynamic_sidebar('jemma-module'); ?>
               

            <?php endif; ?>
        </div>
    </div>
</div>

