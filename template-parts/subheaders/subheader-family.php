<?php 
 $image_path = wp_upload_dir();
if(is_category('family')): ?>
<div class="subheader">
    <div class="hero large-purple-corner">
        <div class="post-corner"></div>
        <div class="hero-left-interior" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-2016-bg.jpg)">
            <div class="inner-hero-content">
                <h1>
                <?php if (is_single()) {
                echo '';
                } else {
                single_cat_title();
                }
                ?>
                
                </h1>
            </div>
        </div>
        <div class="hero-right">
            <?php if ( is_active_sidebar( 'jemma-module' ) ) : ?>
            <?php dynamic_sidebar('jemma-module'); ?>
            <?php endif; ?>
        </div>
    </div>
</div>


<?php elseif (is_single('best-sports-parenting-advice-i-gave-myself')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/dec2016/CP-USL-0007-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('unhappy-holidays')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/dec2016/CP-GBMC-0008-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('five-reasons-not-to-put-off-estate-planning')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/dec2016/CP-PKL-0001-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('win-parenting-by-showing-up')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-0007-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('instagram-according-teenagers')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-V-0003.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('snapchat-according-teenagers')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-V-0003.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('facebook-according-teenagers')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-V-0003.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('twitter-according-teenagers')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-V-0003.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('thank-you')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/thank-you-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('forgiveness-your-health-depends-on-it')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-JHM-0008-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('your-teenagers-first-job-how-they-should-spend-their-money')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/F-0011.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('what-really-matters-in-player-development')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-USL-0004.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>



<?php elseif (is_single('the-whys-of-multi-sport-participation')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-USL-0003.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('does-your-teen-have-a-fake-instagram-account-and-would-you-know')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-0005.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('as-a-parent-im-free-range-chicken')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-0004.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('the-case-for-a-graduated-smartphone-license')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-0003.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('talking-to-your-teen-about-social-media')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-0001.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('will-you-be-shut-out-if-your-college-age-child-has-a-medical-emergency')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-PKL-0005.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>



<?php elseif (is_single('still-havent-prepared-a-will-you-could-be-leaving-a-mess-spouse-partner')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-PKL-0002.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>



<?php elseif (is_single('supporting-a-spouse-through-a-health-challenge')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-JHM-0011.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>



<?php elseif (is_single('fall-activities-to-get-kids-moving')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-GBMC-0003.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('the-friend-who-keeps-young')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-JHM-0009-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('advice-from-a-14-year-old')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-0006-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('parents-enjoy-moment')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-USL-0006.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>




<?php elseif (is_single('should-your-child-get-allowance')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/F-0006-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('raising-generation-k')): ?>

<div class="subheader">
    <div class="hero large-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/family-headers/CP-RK-0002-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php // Checking if author page, then show another subheader. WP keeps pulling the wrong file :P
    elseif(is_author()): 
    get_template_part('template-parts/subheaders/subheader', 'author');
?>
    

<?php else: ?>

<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/header-family-full.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>
    


<?php endif; ?>