<?php
    $image_path = wp_upload_dir();
if(is_home('news')): ?> 

<div class="subheader">
    <div class="hero">

        <div class="hero-left-interior" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/news-2016-bg.jpg)">
            <div class="inner-hero-content">
            <h1>News</h1>
            </div>
        </div>

        <div class="hero-right">
            <?php if ( is_active_sidebar( 'jemma-module' ) ) : ?>
                <div>
                    <?php dynamic_sidebar('jemma-module'); ?>
                </div><!-- #primary .aside -->

            <?php endif; ?>
        </div>
        
    </div>
</div>


<?php elseif(is_single('jemma-everyday-announces-new-partner-pandora-jewelry')): ?>

<div class="subheader">
    <div class="hero" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/header-life-full.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>



<?php else: ?>
    
<div class="subheader">
    <div class="hero">

        <div class="hero-left-interior" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/news-2016-bg.jpg)">
            <div class="inner-hero-content">
            <h1>News</h1>
            </div>
        </div>

        <div class="hero-right">
            <?php if ( is_active_sidebar( 'jemma-module' ) ) : ?>
                <div>
                    <?php dynamic_sidebar('jemma-module'); ?>
                </div><!-- #primary .aside -->

            <?php endif; ?>
        </div>
        
    </div>
</div>




<?php endif; ?>