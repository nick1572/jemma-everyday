<?php 
 $image_path = wp_upload_dir();
if(is_category('life')): ?>
<div class="subheader">
    <div class="hero large-dk-purple-corner">
        <div class="post-corner"></div>
        <div class="hero-left-interior" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-2016-bg.jpg)">
            <div class="inner-hero-content">
                <h1>
                <?php if (is_single()) {
                echo '';
                } else {
                single_cat_title();
                }
                ?>
                
                </h1>
            </div>
        </div>
        <div class="hero-right">
            <?php if ( is_active_sidebar( 'jemma-module' ) ) : ?>
            <?php dynamic_sidebar('jemma-module'); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php elseif (is_single('giving-back')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/dec2016/CP-PAN-0001-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('rev-up-your-car-knowledge')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/2016/12/L-0007-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('romance-in-the-city-san-fran')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/2016/12/CP-PC-0002-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('keep-your-resolutions')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/dec2016/L-0004-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('stress-management-at-any-age')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-JHM-0005-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('adjusting-parent-attitudes-toward-officials')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-USL-0008-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('gaining-strength-safely-and-efficiently')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-EIF-0002-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('five-ways-to-ease-travel-stress-before-you-go-abroad')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/L-0001.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('seven-apps-to-make-holidays-brighter')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/dec2016/L-0005-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('10-reasons-coaching-will-improve-life')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-USL-0001.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('depression-his-vs-hers')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-JHM-0007-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('how-to-cope-with-a-later-life-crisis')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-JHM-0006.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('yogas-benefits-head-toe')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-JHM-0003.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('choosing-healthy-cooking-oils')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-GBMC-0012.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('speak-breast-cancer')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-GBMC-0007.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>



<?php elseif (is_single('listen-up-earbud-safety')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-GBMC-0005.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('considering-using-a-personal-trainer')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-EIF-0001-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('seven-ways-to-get-a-healthier-nights-sleep')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-JHM-0004.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('athletic-passion-gender-neutral')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-USL-0002.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('red-wine-good-for-the-heart-mind-and-body')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-GBMC-0002-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('six-dos-and-donts-for-youth-coaches')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-USL-0005-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>



<?php elseif (is_single('4-tips-for-healthy-grocery-shopping')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-GBMC-0006-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('15-minute-recipes')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-McCormick-pursuit-of-pure-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('new-years-recipes')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-McCormick-pursuit-of-pure-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php elseif (is_single('the-pursuit-of-pure-healthy-recipes-for-kids')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-McCormick-pursuit-of-pure-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>



<?php elseif (is_single('real-solutions-to-combat-holiday-stress')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/L-0003-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>


<?php elseif (is_single('lost-recipes')): ?>
    
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/life-headers/CP-McCormick-pursuit-of-pure-header.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>



<?php // Checking if author page, then show another subheader. WP keeps pulling the wrong file :P
    elseif (is_author()): 
    get_template_part('template-parts/subheaders/subheader', 'author');
?>

<?php else: ?>
<div class="subheader">
    <div class="hero large-dk-purple-corner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/header-life-full.jpg)">
        <div class="post-corner"></div>
        <div class="hero-left-interior-full">
            
            <h1><?php single_cat_title(); ?></h1>
            
        </div>
    </div>
</div>

<?php endif; ?>
