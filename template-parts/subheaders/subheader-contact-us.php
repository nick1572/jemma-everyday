<?php
    $image_path = wp_upload_dir();
?>
<div class="subheader">
    <div class="hero">
  
        <div class="hero-left-interior" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/contact-2016-bg.jpg)">
            <div class="inner-hero-content">
                <h1>
                    <?php  echo 'Contact';
                    ?>
                        
                </h1>
            </div>
        </div>
        <div class="hero-right">
            <?php if ( is_active_sidebar( 'jemma-module' ) ) : ?>
               
                    <?php dynamic_sidebar('jemma-module'); ?>
               

            <?php endif; ?>
        </div>
    </div>
</div>