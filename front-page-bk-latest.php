<?php


get_header('new'); 
global $post;

?>

    <?php
    $image_path = wp_upload_dir();
?>


        <div class="primary-full">

 <div class="post-container">




                <div class="featured-post-third">
                    <div class="featured-thumb-third">
                        <div class="post-corner"></div>

                        <div class="post-cover">

                            <?php if ( $caption = get_post( get_post_thumbnail_id(1602) )->post_excerpt ) : ?>
                            <p><a href="<?php the_permalink(1602); ?>"><?php echo $caption; ?></a></p>
                            <?php else: ?>
                            <p class="thumb-generic-linker"><a href="<?php the_permalink(1602); ?>"><?php echo get_the_title(1602); ?><i class="fa fa-link" aria-hidden="true"></i></a></p>
                            <?php endif ; ?>
                        
                        </div>

                         
                            <?php echo get_the_post_thumbnail(1602);?>
                        

                    </div>
                    <div class="featured-title">
                        <h2 class="widget-title"><a href="<?php the_permalink(1602); ?>"><?php echo get_the_title(1602);?></a></h2>
                    </div>
                </div>







                <div class="featured-post-third">
                    <div class="featured-thumb-third blue-corner">
                        <div class="post-corner"></div>

                        <div class="post-cover">

                            <?php if ( $caption = get_post( get_post_thumbnail_id(1356) )->post_excerpt ) : ?>
                            <p><a href="<?php the_permalink(1356); ?>"><?php echo $caption; ?></a></p>
                            <?php else: ?>
                            <p class="thumb-generic-linker"><a href="<?php the_permalink(1356); ?>"><?php echo get_the_title(1356); ?><i class="fa fa-link" aria-hidden="true"></i></a></p>
                            <?php endif ; ?>
                        
                        </div>

                         
                            <?php echo get_the_post_thumbnail(1356);?>
                        

                    </div>
                    <div class="featured-title">
                        <h2 class="widget-title"><a href="<?php the_permalink(1356); ?>"><?php echo get_the_title(1356);?></a></h2>
                    </div>
                </div>

                <div class="featured-post-third">
                    <div class="featured-thumb-third blue-corner">
                        <div class="post-corner"></div>


                        <div class="post-corner"></div>

                        <div class="post-cover">

                            <?php if ( $caption = get_post( get_post_thumbnail_id(472) )->post_excerpt ) : ?>
                            <p><a href="<?php the_permalink(472); ?>"><?php echo $caption; ?></a></p>
                            <?php else: ?>
                            <p class="thumb-generic-linker"><a href="<?php the_permalink(472); ?>"><?php echo get_the_title(472); ?><i class="fa fa-link" aria-hidden="true"></i></a></p>
                            <?php endif ; ?>
                        
                        </div>



                            <?php echo get_the_post_thumbnail(472);?>
                        

                    </div>
                    <div class="featured-title">
                        <h2 class="widget-title"><a href="<?php the_permalink(472); ?>"><?php echo get_the_title(472);?></a></h2>
                    </div>
                </div>

                <div class="featured-post-third">
                    <div class="featured-thumb-third purple-corner">
                        <div class="post-corner"></div>


                        <div class="post-corner"></div>

                        <div class="post-cover">

                            <?php if ( $caption = get_post( get_post_thumbnail_id(1227) )->post_excerpt ) : ?>
                                <p>
                                    <a href="<?php the_permalink(1227); ?>">
                                        <?php echo $caption; ?>
                                    </a>
                                </p>
                                <?php endif ; ?>
                        </div>


                        <a href="<?php the_permalink(1227); ?>">
                            <?php echo get_the_post_thumbnail(1227);?>
                        </a>

                    </div>
                    <div class="featured-title">
                        <h2 class="widget-title"><a href="<?php the_permalink(1227); ?>"><?php echo get_the_title(1227);?></a></h2>
                    </div>
                </div>
            </div>

        


            <div class="post-container">

                <div class="featured-post-half">
                    <div class="featured-thumb-third dk-purple-corner">
                        <div class="post-corner"></div>


                        <div class="post-corner"></div>

                        <div class="post-cover">

                            <?php if ( $caption = get_post( get_post_thumbnail_id(1180) )->post_excerpt ) : ?>
                                <p>
                                    <a href="<?php the_permalink(1180); ?>">
                                        <?php echo $caption; ?>
                                    </a>
                                </p>
                                <?php endif ; ?>
                        </div>



                        <a href="<?php the_permalink(1180); ?>"><img src="<?php echo $image_path['baseurl']; ?>/2016/11/CP-RK-V-0003-480x270.jpg"></a>

                    </div>
                    <div class="featured-title">
                        <h2 class="widget-title"><a href="<?php the_permalink(1180); ?>"><?php echo get_the_title(1180);?></a></h2>
                    </div>
                </div>
              
              
 
                <div class="featured-post-half">
                    <div class="featured-thumb-third dk-purple-corner">
                        <div class="post-corner"></div>


                        <div class="post-corner"></div>

                        <div class="post-cover">

                            <?php if ( $caption = get_post( get_post_thumbnail_id(1081) )->post_excerpt ) : ?>
                                <p>
                                    <a href="<?php the_permalink(1081); ?>">
                                        <?php echo $caption; ?>
                                    </a>
                                </p>
                                <?php endif ; ?>
                        </div>



                        <a href="<?php the_permalink(1081); ?>"><img src="<?php echo $image_path['baseurl']; ?>/featured-images/CP-GBMC-0002-480x270.jpg"></a>

                    </div>
                    <div class="featured-title">
                        <h2 class="widget-title"><a href="<?php the_permalink(1081); ?>"><?php echo get_the_title(1081);?></a></h2>
                    </div>
                </div>

            </div>





          


            <div class="partners">
                <div class="slider-row">
                    <?php dynamic_sidebar('partners') ?>
                </div>
            </div>


                <div class="post-container">
                <div class="post">
                    <div class="post-thumb purple-corner">
                        <div class="post-corner"></div>

                        <div class="post-cover">

                            <?php if ( $caption = get_post( get_post_thumbnail_id(1420) )->post_excerpt ) : ?>
                            <p><a href="<?php the_permalink(1420); ?>"><?php echo $caption; ?></a></p>
                            <?php else: ?>
                            <p class="thumb-generic-linker"><a href="<?php the_permalink(1420); ?>"><?php echo get_the_title(1420); ?><i class="fa fa-link" aria-hidden="true"></i></a></p>
                            <?php endif ; ?>
                        
                        </div>
                        <?php echo get_the_post_thumbnail(1420);?>
                    </div>

                    <div class="post-content">
                        <h2 class="widget-title"><a href="<?php the_permalink(1420); ?>"><?php echo get_the_title(1420);?></a></h2>
                        <p>
                            Every year, Thanksgiving sneaks up on us somewhere between the Halloween décor clearance sales and the release of the annual Christmas shopping extravaganza. It can...
                            <p><a href="<?php the_permalink(1420); ?>" class="readmore">Read More &raquo;</a></p>
                        </p>
                    </div>
                </div>
            </div>

           
<div class="post-container">
                <div class="post">
                    <div class="post-thumb">
                        <div class="post-corner"></div>

                        <div class="post-cover">
                            <?php $image_path = wp_upload_dir(); ?> 
                                <p>
                                    <a href="<?php the_permalink(6); ?>" style="padding-top:1rem;display:block !important; min-height:149px; width:100%;">
                                        <img src="<?php echo $image_path['baseurl']; ?>/partner-ko-logos/bb-ko.png"> 
                                    </a>
                                </p>
                                
                        </div>
                        <img src="<?php echo $image_path['baseurl']; ?>/featured-images/olivia-brave-and-beyond-thumbnail-1.jpg"/>
                    </div>

                    <div class="post-content">
                        <h2 class="widget-title"><a href="<?php the_permalink(6); ?>">Brave &amp; Beyond Honors: Olivia Hutcherson</a></h2>
                        <p>
                           Olivia Hutcherson, who was recently featured on "Good Morning America", shares her story about overcoming adversity in Jemma Everyday's first Brave &amp; Beyond feature.
                            <p><a href="<?php the_permalink(6); ?>" class="readmore">Watch Video &raquo;</a></p>
                        </p>
                    </div>
                </div>
            </div>


      


          <div class="post-container">
                    <div class="post">
                        <div class="post-thumb purple-corner">
                            <div class="post-corner"></div>

                            <div class="post-cover">
                                <?php if ( $caption = get_post( get_post_thumbnail_id(513) )->post_excerpt ) : ?>
                                    <p>
                                        <a href="<?php the_permalink(513); ?>">
                                            <?php echo $caption; ?>
                                        </a>
                                    </p>
                                    <?php endif ; ?>
                            </div>
                            <a href="<?php the_permalink(513); ?>"><?php echo get_the_post_thumbnail(513);?></a>
                        </div>

                        <div class="post-content">
                            <h2 class="widget-title"><a href="<?php the_permalink(513); ?>"><?php echo get_the_title(513);?></a></h2>
                            <p>When a spouse is diagnosed with a serious health condition, what can you say? What should you do? A Johns Hopkins expert shares strategies to...<p><a href="<?php the_permalink(513); ?>" class="readmore">Read More &raquo;</a></p></p>
                        </div>
                    </div>
                </div>

                <div class="post-container">
                    <div class="post">
                        <div class="post-thumb purple-corner">
                            <div class="post-corner"></div>

                            <div class="post-cover">
                                <?php if ( $caption = get_post( get_post_thumbnail_id(1094) )->post_excerpt ) : ?>
                                    <p>
                                    <a href="<?php the_permalink(1094); ?>">
                                        <?php echo $caption; ?>
                                    </a>
                                </p>
                                <?php endif ; ?>
                        </div>
                        <a href="<?php the_permalink(1094); ?>"><?php echo get_the_post_thumbnail(1094);?></a>
                    </div>

                    <div class="post-content">
                        <h2 class="widget-title"><a href="<?php the_permalink(1094); ?>"><?php echo get_the_title(1094);?></a></h2>
                        <p>A letter to my former self as a new sports parent: One day you’re going to get in the car with your kid’s water bottle...<p><a href="<?php the_permalink(1094); ?>" class="readmore">Read More &raquo;</a></p></p>
                    </div>
                </div>
            </div>



            <div class="post-container">
                <div class="post">
                    <div class="post-thumb gray-corner">
                        <div class="post-corner"></div>

                        <div class="post-cover">

                            <?php if ( $caption = get_post( get_post_thumbnail_id(652) )->post_excerpt ) : ?>
                            <p><a href="<?php the_permalink(652); ?>"><?php echo $caption; ?></a></p>
                            <?php else: ?>
                            <p class="thumb-generic-linker"><a href="<?php the_permalink(652); ?>"><?php echo get_the_title(652); ?><i class="fa fa-link" aria-hidden="true"></i></a></p>
                            <?php endif ; ?>
                        
                        </div>
                        <?php echo get_the_post_thumbnail(652);?>
                    </div>

                    <div class="post-content">
                        <h2 class="widget-title"><a href="<?php the_permalink(652); ?>"><?php echo get_the_title(652);?></a></h2>
                        <p>Salary negotiations can be intimidating—even downright nerve-wracking. Worse, men seem to do it more often than women, giving them an edge in the workforce...<p><a href="<?php the_permalink(652); ?>" class="readmore">Read More &raquo;</a></p></p>
                    </div>      
                </div>
            </div>







        </div><!--//End Primary Full-->



        <?php get_sidebar(); ?>
            <?php get_footer(); ?>