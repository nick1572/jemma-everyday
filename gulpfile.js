var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require ('gulp-sourcemaps');
//var browserSync = require('browser-sync').create();

gulp.task('sass', function() {
	gulp.src('assets/sass/style.scss')
	.pipe(sass({ outputStyle: 'expanded' }))
	.pipe(autoprefixer())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('./'))
	//.pipe(browserSync.reload({
		//stream:true
	//}))
});


/*gulp.task('browserSync',  function() {
	browserSync.init({
		server: {
			baseDir: './'
		},
		browser: 'google chrome'
	})
});*/


gulp.task('default', ['sass'], function() {
  gulp.watch('assets/sass/**/*.scss', ['sass']);
  gulp.watch('./*html');
 
});


