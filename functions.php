<?php
/**
 * Jemma Everyday functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Jemma_Everyday
 */

if ( ! function_exists( 'jemma_ev_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function jemma_ev_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Jemma Everyday, use a find and replace
	 * to change 'jemma_ev' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'jemma_ev', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'jemma_ev' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'jemma_ev_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'jemma_ev_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jemma_ev_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'jemma_ev_content_width', 640 );
}
add_action( 'after_setup_theme', 'jemma_ev_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */


function jemma_ev_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'jemma_ev' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
    
    
    register_sidebar( array(
		'name'          => esc_html__( 'Homepage Intro', 'jemma_ev' ),
		'id'            => 'hp-intro',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	// Add Header Links Widget Area

	register_sidebar( array(
		'name'          => esc_html__( 'Header Links Widget Area', 'jemma_ev' ),
		'id'            => 'header-inner-links',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Header JF Call Widget Area', 'jemma_ev' ),
		'id'            => 'header-band-call',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	// Add Header Social Icons Widget Area

	register_sidebar( array(
		'name'          => esc_html__( 'Header Social Widget Area', 'jemma_ev' ),
		'id'            => 'header-social-links',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	// Add Footer Widgets Area

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Area', 'jemma_ev' ),
		'id'            => 'footer-widget-area',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s footer-items">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	// Add Subheader right side widget Area

	register_sidebar( array(
		'name'          => esc_html__( 'Right Subheader Widget Area', 'jemma_ev' ),
		'id'            => 'jemma-module',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));



	// Add Explore Row  widget Area

	register_sidebar( array(
		'name'          => esc_html__( 'Explore Row Widget Area', 'jemma_ev' ),
		'id'            => 'explore-more',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));



	// Add Signup Row  widget Area

	register_sidebar( array(
		'name'          => esc_html__( 'Signup Row Widget Area', 'jemma_ev' ),
		'id'            => 'signup-row',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
    
    
    // Partner Logo widget Area

	register_sidebar( array(
		'name'          => esc_html__( 'Partner Logo Widget Area', 'jemma_ev' ),
		'id'            => 'partners',
		'description'   => esc_html__( 'Add widgets here.', 'jemma_ev' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));



}
add_action( 'widgets_init', 'jemma_ev_widgets_init' );




/**
 * Enqueue scripts and styles.
 */
function jemma_ev_scripts() {
	wp_enqueue_style( 'jemma_ev-style', get_stylesheet_uri() );
    wp_enqueue_style( 'thumbnail-slider', get_template_directory_uri() . '/assets/css/thumbnail-slider.css');
    wp_enqueue_style( 'tooltip', get_template_directory_uri() . '/assets/css/tooltip.css');

	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js', array(), null, true );
	wp_enqueue_script( 'jemma_ev-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'jquery-ui.min', get_template_directory_uri() . '/assets/js/jquery-ui.min.js', array(), '20151215', true );
	wp_enqueue_script( 'jquery.magnific-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.js', array(), '20151215', true );
	wp_enqueue_script( 'thumbnail-slider', get_template_directory_uri() . '/assets/js/thumbnail-slider.js', array(), '20151215', true );
	wp_enqueue_script( 'tooltip', get_template_directory_uri() . '/assets/js/tooltip.js', array(), '20151215', true );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js', array(), '20151215', true );

	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-ui.min');
	wp_enqueue_script('jquery.magnific-popup');
	wp_enqueue_script('thumbnail-slider');
	wp_enqueue_script('tooltip');
	wp_enqueue_script('scripts');

	wp_enqueue_script( 'jemma_ev-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'jemma_ev_scripts' );

/**
 * Add Class to the body tag
 *
 *
 */
function add_slug_body_class( $classes ) {
		global $post;
		if ( isset( $post ) ) {
		$classes[] = $post->post_name;
	}
	return $classes;
	}
add_filter( 'body_class', 'add_slug_body_class' );



add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
    if ( is_page_template( 'author.php' ) ) {
        $classes[] = 'example';
    }
    return $classes;
}


add_filter( 'template_include', 'var_template_include', 1000 );
function var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}

function get_current_template( $echo = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
    if( $echo )
        echo $GLOBALS['current_theme_template'];
    else
        return $GLOBALS['current_theme_template'];
}




function add_menuclass($ulclass) {
	return preg_replace('/<a rel="menu-pop"/', '<a rel="menu-pop" class="popup-link"', $ulclass, 1);
		}
add_filter('wp_nav_menu','add_menuclass');


// Custom walker to allow drop down menus to be in columns
class Column_Walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu\">";
		// Add this piece to start to wrap all the <li> tags in a <div class="col">
		if( $depth == 0 ) {
			$output .= "\n$indent<div class=\"col\">";
		}
		// End
		$output .= "\n";
	}
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		// Add this piece to close the wrap of all the <li> tags in a <div class="col">.
		// It should be noted that an extra empty <div> is included to allow for clearing any floated column.
		// Feel free to remove '$indent<div class=\"clear\"></div>' if you have your own clear method
		if( $depth == 0 ) {
			$output .= "$indent</div>\n$indent<div class=\"clear\"></div>";
		}
		// End
		$output .= "$indent</ul>\n";
	}
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' .  esc_attr( $class_names ) . '"';
		// This piece checks to see if you added a class of 'col-break' to any menu item in the admin area.
		// If you did, it breaks the column and starts a new one BEFORE the item is displayed
		if( stripos( $class_names, 'col-break' ) !== false )
			$output .= '</div><div class="col">';
		// End
		$output .= $indent . '<li id="menu-item-' .  $item->ID . '"' . $value . $class_names . '>';
		$attributes  = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		$item_output = $args->before;
		$item_output .= '<a' . $attributes . '>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}


/*add_filter('single_template', 'check_for_category_single_template');
function check_for_category_single_template( $t )
{
  foreach( (array) get_the_category() as $cat )
  {
    if ( file_exists(TEMPLATEPATH . "/single-category-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-category-{$cat->slug}.php";
    if($cat->parent)
    {
      $cat = get_the_category_by_ID( $cat->parent );
      if ( file_exists(TEMPLATEPATH . "/single-category-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-category-{$cat->slug}.php";
    }
  }
  return $t;
}*/


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


add_filter( 'get_the_archive_title', 'remove_archive_title_prefix', 10, 1 );

function remove_archive_title_prefix( $title ) {

    if ( is_category() ) {
		$title = single_cat_title( '', false );
    }

    return $title;
}

function custom_widget_link( $title ) {

// assume there's a link attached to the title because it starts with ww, http, or /
	if ( ( substr( $title, 0, 4) == "www." ) || ( substr( $title, 0, 4) == "http" ) || ( substr( $title, 0, 1) == "/" ) ) {

// split our title in half
		$title_pieces = explode( "|", $title );

// if there's two pieces
		if ( count( $title_pieces ) == 2 ) {

// add http if it's just www
			if ( substr( $title, 0, 4) == "www." ) {
				$title_pieces[0] = str_replace( "www.", "http://www.", $title_pieces[0] );
			}

// create new title from url and extracted title
			$title = '<a href="' . $title_pieces[0] . '" title="' . $title_pieces[1] . '">' . $title_pieces[1] . '</a>';
		}
	}

return $title;
}
add_filter( "widget_title", "custom_widget_link" );


		// Replaces the excerpt "Read More" text by a link
	function new_excerpt_more($more) {
		/*$thePostID == 6 || $thePostID == 576 || $thePostID == 1180 || $thePostID == 1184 || $thePostID == 1128 || $thePostID == 1193 || $thePostID == 1378 || $thePostID == 1380 || $thePostID == 1390 || $thePostID == 1387*/
	    global $post;
	    $thePostID = $post->ID;
	    if(has_tag(array('corporate-partner-video', 'speaker-video'))) {
		return '...<p><a class="readmore" href="'. get_permalink($post->ID) . '">Watch Video &raquo;</a><p>';
		} else {
		return '...<p><a class="readmore" href="'. get_permalink($post->ID) . '">Read More &raquo;</a></p>';
		}
	}
	add_filter('excerpt_more', 'new_excerpt_more');




function wpdocs_custom_excerpt_length( $length ) {
    return 25;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

// custom excerpt outside the loop



if (class_exists('MultiPostThumbnails')) {
	new MultiPostThumbnails(array(
	'label' => 'Secondary Image',
	'id' => 'secondary-image',
	'post_type' => 'post'
	 ));
}

// Filter SEO and Co Authors Plus Plugin to show the Coauthor in the Page Title

add_filter('wpseo_title', 'my_co_author_wseo_title');
function my_co_author_wseo_title( $title ) {
	
	// Only filter title output for author pages
	if ( is_author() && function_exists( 'get_coauthors' ) ) {
		$qo = get_queried_object();
		$author_name = $qo->display_name;
		return $author_name . ', Author at ' . get_bloginfo('name');
	}
	return $title;
	
}


/**
* Define a constant path to our single template folder
*/
define(SINGLE_PATH, TEMPLATEPATH . '/single');

/**
* Filter the single_template with our custom function
*/
add_filter('single_template', 'my_single_template');

/**
* Single template function which will choose our template
*/
function my_single_template($single) {
	global $wp_query, $post;

	/**
	* Checks for single template by author
	* Check by user nicename and ID
	*/
	$curauth = get_userdata($wp_query->post->post_author);

	if(file_exists(SINGLE_PATH . '/single-author-' . $curauth->user_nicename . '.php'))
		return SINGLE_PATH . '/single-author-' . $curauth->user_nicename . '.php';

	elseif(file_exists(SINGLE_PATH . '/single-author-' . $curauth->ID . '.php'))
		return SINGLE_PATH  . '/single-author-' . $curauth->ID . '.php';

	return $single;

}






